# OpenAPI

The OpenAPI (Swagger) Adventures!

1. API definition (https://swagger.io/docs/specification/about/)
2. Server Codebase generation
3. Client generation


## API Definition using Visual Studio Code 

Extension: https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi

### Create new OpenAPI files

1. Press Ctrl+Shift+P on Windows or Linux, or Cmd+Shift+P on a Mac.
2. In the command prompt, start typing new openapi, and click the corresponding command to create either an OAS v2 or v3 template file.
3. Use the OpenAPI explorer to populate the template with new paths and other elements as needed.
4. Save the file to your disk to fully enable IntelliSense.


## Code Generation

npm install @openapitools/openapi-generator-cli -g

usage:
openapi-generator generate -i <openapi-file> -g <generator-id> -o <output-path>

